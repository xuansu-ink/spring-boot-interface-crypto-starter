package com.xuansu.crypto.enums;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 17:39
 * @des 加密类型
 */
public enum EncrptyTypeEnum {
    DES, AES, RSA, BASE64, SM2, SM4
}
