package com.xuansu.crypto.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 16:41
 * @des 请求参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class RequestData implements Serializable {
    private String data;
}
