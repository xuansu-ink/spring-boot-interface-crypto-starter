package com.xuansu.crypto.entity;

import com.xuansu.crypto.enums.EncrptyTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/13 10:00
 * @des
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EncryptEntity {
    private EncrptyTypeEnum encrptyTypeEnum;
    private String key;
}
