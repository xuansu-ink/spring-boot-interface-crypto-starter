package com.xuansu.crypto.entity;

import lombok.*;
import org.springframework.http.HttpStatus;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 16:18
 * @des
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class Result<T> {
    @Builder.Default
    private String code = HttpStatus.OK.toString();
    private T data;
    @Builder.Default
    private boolean success = true;
    @Builder.Default
    private String message = "成功";
    @Builder.Default
    private Long timestamp = System.currentTimeMillis();
}
