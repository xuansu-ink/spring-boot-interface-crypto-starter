package com.xuansu.crypto.entity;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author 何志华
 * @description 返回值构造
 */
public interface ResultBuilder {

    /**
     * 成功的构造
     *
     * @param data 数据
     * @return Result
     */
    default ResponseEntity<Result<?>> success(Object data) {
        return ResponseEntity.ok(Result.builder()
                .code(Integer.toString(HttpStatus.OK.value())).data(data)
                .build());
    }

    /**
     * 400的构造
     *
     * @param errorMsg 错误信息
     * @return Result
     */
    default ResponseEntity<Result<?>> badRequest(String errorMsg) {
        return ResponseEntity.badRequest().body(Result.builder()
                .code(Integer.toString(HttpStatus.BAD_REQUEST.value()))
                .success(false)
                .message(errorMsg)
                .build());
    }

    /**
     * 404的构造
     *
     * @param errorMsg 错误信息
     * @return Result
     */
    default ResponseEntity<Result<?>> notFound(String errorMsg) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND.value())
                .body(Result.builder()
                        .code(Integer.toString(HttpStatus.NOT_FOUND.value()))
                        .success(false)
                        .message(errorMsg)
                        .build());
    }

    /**
     * 500的构造
     *
     * @param errorMsg 错误信息
     * @return Result
     */
    default ResponseEntity<Result<?>> error(String errorMsg) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .body(Result.builder()
                        .code(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                        .success(false)
                        .message(errorMsg)
                        .build());
    }

}
