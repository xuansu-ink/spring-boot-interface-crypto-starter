package com.xuansu.crypto.annotation.decrypt;

import com.xuansu.crypto.enums.EncrptyTypeEnum;

import java.lang.annotation.*;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 17:50
 * @des
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Decrypt {
    EncrptyTypeEnum type() default EncrptyTypeEnum.BASE64;

    String value() default "";
}
