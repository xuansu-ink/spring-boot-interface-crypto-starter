package com.xuansu.crypto.annotation.decrypt;

import java.lang.annotation.*;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 17:33
 * @des base64解密注解
 */
//类和方法上
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Base64Decrypt {
}
