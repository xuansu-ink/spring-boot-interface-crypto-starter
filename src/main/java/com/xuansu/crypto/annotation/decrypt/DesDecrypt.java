package com.xuansu.crypto.annotation.decrypt;

import java.lang.annotation.*;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 17:55
 * @des
 */
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DesDecrypt {
    String key() default "";
}
