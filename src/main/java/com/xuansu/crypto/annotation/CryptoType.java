package com.xuansu.crypto.annotation;

import java.lang.annotation.*;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:22
 * @des 加解密类型注解
 */
@Target(value = {ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CryptoType {
    String value() default "";
}
