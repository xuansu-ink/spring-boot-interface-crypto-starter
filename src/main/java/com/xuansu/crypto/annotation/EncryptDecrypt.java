package com.xuansu.crypto.annotation;


import java.lang.annotation.*;

@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EncryptDecrypt {
    public String value() default "BASE64";
}
