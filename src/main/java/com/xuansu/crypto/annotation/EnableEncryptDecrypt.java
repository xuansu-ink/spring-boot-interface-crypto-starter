package com.xuansu.crypto.annotation;

import com.xuansu.crypto.advice.DecryptRequestBodyAdvice;
import com.xuansu.crypto.advice.EncryptResponseAdvice;
import com.xuansu.crypto.exception.CryptoGlobalExceptionAdvice;
import com.xuansu.crypto.processor.CryptoBeanFactoryPostProcessor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 18:01
 * @des
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(value = {CryptoBeanFactoryPostProcessor.class, DecryptRequestBodyAdvice.class, EncryptResponseAdvice.class, CryptoGlobalExceptionAdvice.class})
public @interface EnableEncryptDecrypt {
}
