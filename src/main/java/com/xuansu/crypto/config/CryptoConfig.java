package com.xuansu.crypto.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author hzhh123
 * 自动配置类
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@EnableConfigurationProperties(CryptoProperties.class)
@Configuration
@ComponentScan(basePackages = {"com.xuansu.crypto.handle"})
public class CryptoConfig {


}
