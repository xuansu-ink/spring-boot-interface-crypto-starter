package com.xuansu.crypto.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/12 17:46
 * @des 加密配置
 */
@Data
@ConfigurationProperties(prefix = "crypto", ignoreUnknownFields = true)
public class CryptoProperties {
    /**
     * 加解密配置
     */
    private List<PairKey> configs;
    /**
     * 扫描加解密包
     */
    private String scanOuterPackage;

    @Data
    public static class PairKey {
        /**
         * 加密类型
         */
        private String type;
        /**
         * 对称加解密KEY
         */
        private String key;
        /**
         * 非对称加密公钥
         */
        private String publicKey;

        /**
         * 非对称加密私钥
         */
        private String privateKey;
        /**
         * 盐
         */
        private String salt;
        /**
         * 加密模式
         */
        private String mode;
        /**
         * 加密结果类型：HEX和BASE64
         */
        private String resultType = "BASE64";
        /**
         * 编码
         */
        private String encoding = "UTF-8";

    }
}
