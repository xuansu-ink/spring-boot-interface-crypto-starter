package com.xuansu.crypto.exception;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/13 9:18
 * @des
 */
public class DecryptException extends RuntimeException {

    public DecryptException() {
        super("Decrypting data failed. (解密数据失败)");
    }

    public DecryptException(String message) {
        super(message);
    }
}
