package com.xuansu.crypto.exception;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/13 9:18
 * @des
 */
public class EncryptException extends RuntimeException {

    public EncryptException() {
        super("Encrypting data failed. (加密数据失败)");
    }

    public EncryptException(String message) {
        super(message);
    }
}
