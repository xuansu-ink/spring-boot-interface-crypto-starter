package com.xuansu.crypto.utils;

import com.xuansu.crypto.config.CryptoProperties;

import java.util.List;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/16 16:05
 * @des
 */
public class CryptoPropertiesUtils {
    /**
     * 获取加解密属性
     *
     * @param type
     * @param cryptoProperties
     * @return
     */
    public static CryptoProperties.PairKey getCryptoPropertiesPairKey(String type, CryptoProperties cryptoProperties) {
        List<CryptoProperties.PairKey> configs = cryptoProperties.getConfigs();
        if (configs != null && configs.size() > 0) {
            for (CryptoProperties.PairKey config : configs) {
                if (config.getType() == null || "".equals(config.getType())) {
                    return null;
                }
                if (type.toUpperCase().equals(config.getType().toUpperCase())) {
                    return config;
                }
            }
        }
        return null;
    }
}
