package com.xuansu.crypto.handle;

import com.xuansu.crypto.annotation.CryptoType;
import com.xuansu.crypto.config.CryptoProperties;
import com.xuansu.crypto.exception.EncryptException;
import com.xuansu.crypto.utils.CryptoPropertiesUtils;
import com.xuansu.crypto.utils.Sm2Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:34
 * @des
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CryptoType("SM2")
@Component
public class Sm2CryptoHandle extends AbstractCrypto {
    private final CryptoProperties cryptoProperties;


    @Override
    public String encrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return Sm2Utils.encrypt(str, cryptoPropertiesPairKey.getPublicKey(), cryptoPropertiesPairKey.getResultType());
    }

    @Override
    public String decrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return Sm2Utils.decrypt(str, cryptoPropertiesPairKey.getPrivateKey(), cryptoPropertiesPairKey.getResultType());
    }

    private CryptoProperties.PairKey getCryptoPropertiesPairKey() {
        CryptoProperties.PairKey cryptoPropertiesPairKey = CryptoPropertiesUtils.getCryptoPropertiesPairKey("SM2", cryptoProperties);
        if (cryptoPropertiesPairKey == null) {
            throw new EncryptException("SM2加解密配置出错");
        }
        if (cryptoPropertiesPairKey.getPublicKey() == null) {
            throw new EncryptException("SM2公钥不能为空");
        }
        if (cryptoPropertiesPairKey.getPrivateKey() == null) {
            throw new EncryptException("SM2私钥不能为空");
        }
        return cryptoPropertiesPairKey;
    }
}
