package com.xuansu.crypto.handle;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:25
 * @des 加解密抽象类
 */
public abstract class AbstractCrypto {
    public abstract String encrypt(String str);

    public abstract String decrypt(String str);
}
