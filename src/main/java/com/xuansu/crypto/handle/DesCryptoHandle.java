package com.xuansu.crypto.handle;

import com.xuansu.crypto.annotation.CryptoType;
import com.xuansu.crypto.config.CryptoProperties;
import com.xuansu.crypto.exception.EncryptException;
import com.xuansu.crypto.utils.CryptoPropertiesUtils;
import com.xuansu.crypto.utils.DesUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:34
 * @des
 */
@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CryptoType("DES")
public class DesCryptoHandle extends AbstractCrypto {
    private final CryptoProperties cryptoProperties;


    @Override
    public String encrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return DesUtils.encrypt(str, cryptoPropertiesPairKey.getKey());
    }

    @Override
    public String decrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return DesUtils.decrypt(str, cryptoPropertiesPairKey.getKey());
    }

    private CryptoProperties.PairKey getCryptoPropertiesPairKey() {
        CryptoProperties.PairKey cryptoPropertiesPairKey = CryptoPropertiesUtils.getCryptoPropertiesPairKey("DES", cryptoProperties);
        if (cryptoPropertiesPairKey == null) {
            throw new EncryptException("AES加密KEY不能为空");
        }
        if (cryptoPropertiesPairKey.getKey() == null) {
            throw new EncryptException("AES加密KEY不能为空");
        }
        return cryptoPropertiesPairKey;
    }
}
