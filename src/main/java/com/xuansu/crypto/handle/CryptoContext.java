package com.xuansu.crypto.handle;

import cn.hutool.extra.spring.SpringUtil;

import java.util.Map;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:27
 * @des
 */
public class CryptoContext {
    private Map<String, Class<?>> clazzMap;

    public CryptoContext(Map<String, Class<?>> clazzMap) {
        this.clazzMap = clazzMap;
    }

    public AbstractCrypto getInstance(String value) {
        Class<?> clazz = clazzMap.get(value);
        if (clazz == null) {
            throw new IllegalArgumentException(value + "加密类未注册");
        }
        return (AbstractCrypto) SpringUtil.getBean(clazz);
    }
}
