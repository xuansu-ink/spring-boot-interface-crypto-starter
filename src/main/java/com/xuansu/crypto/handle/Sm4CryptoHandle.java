package com.xuansu.crypto.handle;

import com.xuansu.crypto.annotation.CryptoType;
import com.xuansu.crypto.config.CryptoProperties;
import com.xuansu.crypto.exception.EncryptException;
import com.xuansu.crypto.utils.CryptoPropertiesUtils;
import com.xuansu.crypto.utils.Sm4Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:34
 * @des
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CryptoType("SM4")
@Component
public class Sm4CryptoHandle extends AbstractCrypto {
    private final CryptoProperties cryptoProperties;


    @Override
    public String encrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return Sm4Utils.encrypt(str, cryptoPropertiesPairKey.getKey());
    }

    @Override
    public String decrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return Sm4Utils.decrypt(str, cryptoPropertiesPairKey.getKey());
    }


    private CryptoProperties.PairKey getCryptoPropertiesPairKey() {
        CryptoProperties.PairKey cryptoPropertiesPairKey = CryptoPropertiesUtils.getCryptoPropertiesPairKey("SM4", cryptoProperties);
        if (cryptoPropertiesPairKey == null) {
            throw new EncryptException("SM4加密KEY不能为空");
        }
        if (cryptoPropertiesPairKey.getKey() == null) {
            throw new EncryptException("SM4加密KEY不能为空");
        }
        return cryptoPropertiesPairKey;
    }
}
