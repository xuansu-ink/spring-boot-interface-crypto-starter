package com.xuansu.crypto.handle;

import cn.hutool.core.codec.Base64;
import com.xuansu.crypto.annotation.CryptoType;
import org.springframework.stereotype.Component;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 16:34
 * @des
 */
@Component
@CryptoType("BASE64")
public class Base64CryptoHandle extends AbstractCrypto {


    @Override
    public String encrypt(String str) {
        return Base64.encode(str, "UTF-8");
    }

    @Override
    public String decrypt(String str) {
        return Base64.decodeStr(str, "UTF-8");
    }


}
