# spring-boot-interface-crypto-starter

## 介绍
springboot实现对RESTful API接口进行统一加解密

## 实现思路
1. 设置启动加解密API请求或响应(请求解密，响应加密)
2. 编写`RequestBodyAdvice`及`ResponseBodyAdvice`实现,`RequestBodyAdvice`可以在方法入参前拦截请求,
`ResponseBodyAdvice`可以在请求后拦截响应内容
3. supports引入是否加解密
4. 实现`beforeBodyRead` `beforeBodyWrite`
5. 不同方式实现加解密(动态key)

说明：由于利用了`RequestBodyAdvice`及`ResponseBodyAdvice`拦截请求前后，所以请求参数必须有`@RequestBody`注解,请求方法上的注解必须包含`@ResponseBody`
注解，原因是`RequestBodyAdvice`和`ResponseBodyAdvice`,前者拦截不到无@RequestBody的方法，后者拦截不到无@ResponseBody的方法

## 加密解密支持
- 可进行加密的方式有：
    - - [x] AES
    - - [x] DES
    - - [x] RSA
    - - [x] BASE64
    - - [x] SM2
    - - [x] SM4
- 可进行解密的方式有：
    - - [x] AES
    - - [x] DES
    - - [x] RSA
    - - [x] BASE64
    - - [x] SM2
    - - [x] SM4
    
## 使用方法
- 依赖  
`maven仓库`
```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```
`依赖`
```xml
<dependency>
    <groupId>com.gitee.hzhh123</groupId>
    <artifactId>spring-boot-interface-crypto-starter</artifactId>
    <version>1.0.1</version>
</dependency>
```
- 参数配置
在项目的`application.yml`或`application.properties`文件中进行参数配置，例如：
```yaml
crypto:
  configs:
    - type: aes
      key: 2691b31aa11bee4b62c05537aa67558a
    - type: des
      key: 9e07b5136216fd6b
    - type: rsa
      public-key: MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgQq2HopByR9b5Pcj4IMSzF5F7YUeq0RGd/oIlzkyNHxUqV6FXEzTCPmSA+P0r6b9zSF2fVwXdDIy2kTTNffFQvUfP/eEUBw9ULFl4BhEygkQCInCTISkQs8Sx24jDQyY1P0IzkWjjjYtka2/bWcu9euPDAagZl6/KPMM8kI9RPwIDAQAB
      private-key: MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKBCrYeikHJH1vk9yPggxLMXkXthR6rREZ3+giXOTI0fFSpXoVcTNMI+ZID4/Svpv3NIXZ9XBd0MjLaRNM198VC9R8/94RQHD1QsWXgGETKCRAIicJMhKRCzxLHbiMNDJjU/QjORaOONi2Rrb9tZy71648MBqBmXr8o8wzyQj1E/AgMBAAECgYAaH/SA3V/VuV9Sfx9xT4oxNcDaD5gqwO0xx8j4l8JD6RK+tc1P0Ao0Ng6VNcGztGWoyd21OW7zw3V214H3k7XQNQdm1xYFbYuzTN9YokQrEc7zjUSDdxh/f2QuH8N0Btr7FkOT+7vba5f/keim2IcmWNYVrTdCkAOtQnsJvYqBMQJBANQHkG34dTtiSBmTgTAg/1hwCOUSPy8f0Kf57TeHAWC+Pzm4tmYrg9aqU9fvppZaFTiHnmJGvP4AXwExeoHH0m8CQQDBfr6rewFEQCeaqMpDag0vE0LVljCutxsG8ouEIqN0/pzXKpGc99T5eNZZrLvVf+TP3sjaKTxREvaUvM0BrZYxAkEAgcATQUi/LNTq/EPI1dQLjmoY911gLw1QGcsWwFksnbAubrs7W3CboDzhTA5Kqk18GPjdEpTpSKKfgNJvfoXynwJAZvYDjYn5hZDBwjlYz4CKHWeZY7/0jbOfbRX5CUnJQsMNQC1FqInzyP/0x2jz1kqkvbvlkrjogJefoEvKpr7wsQJAPNg/cc71wB4MEUJPJopxR2yr1yS1kO0491iwh21cvR3W8fQMYW59fl6LvwGVfO/BhukZ16+KEnQVvcm2yD/rHQ==
    - type: sm2
      public-key: MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEilivBGPrnunZqZL9Coa+Ir4p63nPgAS95oYFaKYPd+BKHbouiqRMd/WPgwFnZSdPZ1IRrM3TNlHdO65plFwNDw==
      private-key: MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQggz7cN/iSujCLHhGaW8PPHuHiFWHgQJwIVtth/85zQ5GgCgYIKoEcz1UBgi2hRANCAASKWK8EY+ue6dmpkv0Khr4ivinrec+ABL3mhgVopg934Eodui6KpEx39Y+DAWdlJ09nUhGszdM2Ud07rmmUXA0P
      resultType: HEX #设置响应结果加密后是转为HEX还是BASE64,默认转为BASE64
    - type: sm4
      key: ea3f9e5432e4f21d4b01a4e6a6ee95be
    - type: rc2
      key: N1KBLtE9GzFqcEvuyDjBAA==
  scan-outer-package: com.xuansu.sample.crypto #外部扫描加密类

```
加密key的生成使用Sm2Utils.java、Sm4Utils.java、RsaUtils.java、AesUtils.java、DesUtils.java生成,生成的key转base64或hex值，key是hex值的加密后的
值也是hex值，key是base64的则加密后的值也是base64值,其中AES、DES、SM4是对称加密，加解密直接设置key值，publicKey和privateKey值不用设置，非对称加密RSA、SM2需要设置公私钥，key不用设置。   
scan-outer-package是扩展加密方式所扫描的外部包，实现原理是继承了EnvironmentAware,BeanFactoryPostProcessor接口environment通过scan-outer-package获取外部加解密扫描包，代码如下：

```java
//扫描包的实现，实际使用不需要实现这个类,这个类是spring-boot-interface-crypto-starter中的类
package com.xuansu.crypto.processor;

import com.xuansu.crypto.annotation.CryptoType;
import com.xuansu.crypto.handle.CryptoContext;
import com.xuansu.crypto.utils.spring.ClassScanner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 17:00
 * @des
 */
@Slf4j
public class CryptoBeanFactoryPostProcessor implements EnvironmentAware,BeanFactoryPostProcessor {
    private final static String HANDLE_PACKAGE = "com.xuansu.crypto.handle";
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        Map<String, Class<?>> handleMap = new HashMap<>(8);
        ClassScanner.scan(HANDLE_PACKAGE, CryptoType.class).forEach(clazz -> {
            String value = clazz.getAnnotation(CryptoType.class).value();
            handleMap.put(value, clazz);
        });
        //扫描外部包，直接从CryptoProperties中获取不到属性
        String scanOuterCryptoPackage = environment.getProperty("crypto.scan-outer-package");
        if(StringUtils.isNotBlank(scanOuterCryptoPackage)){
            log.info("扫描加解密外部包(crypto.scan-outer-package){}",scanOuterCryptoPackage);
            String[] scanOuterCryptoPackages = scanOuterCryptoPackage.split(",");
            ClassScanner.scan(scanOuterCryptoPackages, CryptoType.class).forEach(clazz -> {
                String value = clazz.getAnnotation(CryptoType.class).value();
                handleMap.put(value, clazz);
            });
        }
        log.info("默认支持加解密方式:{}",handleMap.keySet());
        CryptoContext cryptoContext = new CryptoContext(handleMap);
        configurableListableBeanFactory.registerSingleton(CryptoContext.class.getName(), cryptoContext);
    }


    private ConfigurableEnvironment environment;
    @Override
    public void setEnvironment(Environment environment) {
        this.environment = (ConfigurableEnvironment) environment;
    }
}

```

- 扩展加解密类，继承AbstractCrypto类，实现encryot和decrypt方法

```java
package com.xuansu.sample.crypto;

import com.xuansu.crypto.annotation.CryptoType;
import com.xuansu.crypto.config.CryptoProperties;
import com.xuansu.crypto.exception.EncryptException;
import com.xuansu.crypto.handle.AbstractCrypto;
import com.xuansu.crypto.utils.CryptoPropertiesUtils;
import com.xuansu.crypto.utils.RC2Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/15 21:40
 * @des
 */
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Component
@CryptoType("RC2")
public class RC2CryptoHandle extends AbstractCrypto {
    private final CryptoProperties cryptoProperties;
    @Override
    public String encrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return RC2Utils.encrypt(str,cryptoPropertiesPairKey.getKey());
    }

    @Override
    public String decrypt(String str) {
        CryptoProperties.PairKey cryptoPropertiesPairKey = getCryptoPropertiesPairKey();
        return RC2Utils.decrypt(str,cryptoPropertiesPairKey.getKey());
    }

    private CryptoProperties.PairKey getCryptoPropertiesPairKey() {
        CryptoProperties.PairKey cryptoPropertiesPairKey = CryptoPropertiesUtils.getCryptoPropertiesPairKey("RC2", cryptoProperties);
        if (cryptoPropertiesPairKey == null) {
            throw new EncryptException("RC2加密KEY不能为空");
        }
        if (cryptoPropertiesPairKey.getKey() == null) {
            throw new EncryptException("RC2加密KEY不能为空");
        }
        return cryptoPropertiesPairKey;
    }
}

```
- 启用接口加解密
   启动入口类添加`@EnableEncryptDecrypt`注解开启加解密


- 注解  
`@AesEncrypt`和`@AesDecrypt`是AES算法加密响应报文，解密请求报文   
`@Base64Encrypt`和`@Base64Decrypt`是BASE64算法加密响应报文，解密请求报文   
`@Base64Encrypt`和`@Base64Decrypt`是BASE64算法加密响应报文，解密请求报文   
`@DesEncrypt`和`@DesDecrypt`是DES算法加密响应报文，解密请求报文    
`@RsaEncrypt`和`@RsaDecrypt`是RSA算法加密响应报文，解密请求报文      
`@Encrypt`和`@Decrypt`加密响应报文，解密请求报文,可通过value属性设置要加密的算法和要解密的算法,starter主要实现了DES,AES,RSA,BASE64,SM2,SM4几种算法,
如果要实现其他算法，可继承AbstractCrypto实现新的算法,注意大小写     
`@EncryptDecrypt` 加密响应报文和解密请求报文是同一个加密算法，可通过value属性设置要加密的算法和要解密的算法,starter主要实现了DES,AES,RSA,BASE64,SM2,SM4几种算法,
如果要实现其他算法，可继承AbstractCrypto实现新的算法,注意大小写        

## 其他参考
- [源码-参考](https://gitee.com/licoy/encrypt-body-spring-boot-starter)
- [源码-参考](https://gitee.com/springboot-hlh/spring-boot-csdn.git)

## 测试
### 测试1
[GET]http://localhost:8080/test1   
json请求参数：
```json
{
    "data":"1b400e753dc2cfa01d98caeb23b07172509226bbf24749972486bf44cf35d2b476bb627708ccf9b140f527dd570d3a3e7a0cb9f469a917be229d8181956aef9f"
}
```
响应：
```json
{
    "code": "200",
    "data": "cUpvgQrr2elOOnVFvsqhn6vrxHJWpE7xr5oeoEyVyDy/eQbeosO8g1mDz6WsgVNCWNCpNZdPd7GYy586GwCVFdIUN9N5SICVOTHErgPeqHLGR3fdvjbL6feGrUNbxHTGJhVgwhBq8ljEUoaXXqTpfkwKznBUakgzKlTBa/yZ4bM=",
    "success": true,
    "message": "成功",
    "timestamp": 1650008249034
}
```

请求代码
```java
/**
* 请求参数{"data":"AES加密User实例json数据"}，响应结果RSA加密
*/
@Slf4j
@RestController
@Decrypt(type = EncrptyTypeEnum.AES)
@Encrypt(type = EncrptyTypeEnum.RSA)
public class Test1Controller implements ResultBuilder {

    @GetMapping("/test1")
    public ResponseEntity<Result<?>> test1(@RequestBody User user){
        log.info("{}",user);
        return success(user);
    }
}

/**
 * @author hzhh123
 * @version 1.0
 * @date 2022/4/13 14:47
 * @des
 *
 *
 */
@Slf4j
@RestController
@Decrypt(type = EncrptyTypeEnum.AES)
@Encrypt("SM2")
//@Base64Encrypt
public class Test1Controller implements ResultBuilder {

    @GetMapping("/test1")
    public ResponseEntity<Result<?>> test1(@RequestBody User user){
        log.info("{}",user);
        return success(user);
    }
}
```

### 测试2
   测试采用SM2加解密
[GET]http://localhost:8080/test2/test1
json请求参数：
```json
{
    "data":"BEAgW2lQD7uZjqk5TKgbF7yH0/nk+zBlUSOlrlQgkHLfB0mkcm7iWBVyno18gGIw6YXzw4x+GdZ+51kzT4bAaGYCmkgdoID/VrySn4l48S3uux7OQjllEuleZmsGIxx5FRgsNu1wZwn2SJUj+H0c1CW96E2JtddGt5VBcX8Mgo3bmV9O2BM="
}
```
响应：
```json
{
    "code": "200",
    "data": "BBcDxUJtokuugJ2lIYXe5YuQ7aOTnBa9o8EW5Bu6SnPFdgm7cEiXXHTrZJHElDTSFEGcvT0wbIXbVetkN8mEFwM4X57cm5JzejYaglVFiHuC5CU+wp6bp/QxoAnoRldFboNovA44C7po+AVWFMJI9WucYAOgJuqHKlxFme6X0U6RKRCJAMI=",
    "success": true,
    "message": "成功",
    "timestamp": 1679380118561
}
```

代码
```java
@Slf4j
@RequestMapping("/test2")
@RestController
@EncryptDecrypt(value = "SM2")
public class Test2Controller implements ResultBuilder {

    @GetMapping("/test1")
    public ResponseEntity<Result<?>> test1(@RequestBody User user){
        log.info("{}",user);
        return success(user);
    }

    @EncryptDecrypt
    @GetMapping("/test2")
    public ResponseEntity<Result<?>> test2(@RequestBody User user){
        log.info("{}",user);
        return success(user);
    }
}

```




